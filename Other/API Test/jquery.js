// $(document).ready(function() {
//     $('#slideleft button').click(function() {
//          var $lefty = $(this).next();
//            $lefty.animate({ left: parseInt($lefty.css('left'),10) == 0 ? -$lefty.outerWidth() : 0 }); 
//        }); 
//    });

// $(document).ready(function() {
//     $("#testSlide" ).toggle( "slide" );
// })

$("#city-form").submit(function(event){
    event.preventDefault();

    var cityName = $("#city-name").val();
    getWeather(cityName);
})

function getWeather(city) {
    $.get("http://api.openweathermap.org/data/2.5/weather?q=" + city + "&units=metric&appid=04c5c5619476555e3d88941e956e6911", function(data){
        console.log(data);
        // console.log(data.name);
        // console.log(data.weather[0].description);
        // console.log(data.main.temp);
        $("#name").html(data.name);
        $("#desc").html(data.weather[0].main + " - (" + data.weather[0].description + ")");
        $("#temp").html(data.main.temp + " &#176" + "C");
        $("#press").html(data.main.pressure);
        $("#wind").html(data.wind.speed + " km/h");
    });
}
//http://api.openweathermap.org/data/2.5/weather?q=London&appid=04c5c5619476555e3d88941e956e6911